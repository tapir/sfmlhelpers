package sfmlhelpers

import (
	"fmt"

	sf "gitlab.com/tapir/sfml/v2.3/sfml"
	"gitlab.com/tapir/utime"
)

// A game consists of main elements of a game loop.
// Game loop will keep running until IsRunning returns false.
type Game interface {
	Init(window *sf.RenderWindow)
	Input(event *sf.Event)
	Update(dt float32)
	Draw(alpha float32)
	Quit()
	IsRunning() bool
}

// Window reference is used by game loop to poll for events and draw.
func GameLoop(window *sf.RenderWindow, game Game, disableInfo bool) {
	const (
		ups                 = 30
		minFps              = 4
		dt                  = 1.0 / float64(ups)
		minFpsDelta         = 1.0 / float64(minFps)
		fpsMaxAcc   float64 = 1.0
	)

	var (
		vsync       bool
		showInfo    bool
		lastTime    float64
		accumulator float64
		fpsCounter  int
		fpsAcc      float64
		fpsAverage  float64
		fpsText     *sf.Text
	)

	showInfo = true
	lastTime = utime.Now()
	fpsText = NewDebugText(fmt.Sprintf("VSYNC: %t\nFPS: %.0f", vsync, fpsAverage), 20)
	fpsText.SetPosition(sf.Vector2f{5, 0})

	// Load stuff here
	game.Init(window)

	// Main loop
	for game.IsRunning() {
		newTime := utime.Now()
		frameTime := newTime - lastTime
		if frameTime > minFpsDelta {
			frameTime = minFpsDelta
		}
		lastTime = newTime
		accumulator += frameTime

		// Process input and update
		for accumulator >= dt {
			for event := window.PollEvent(); event != nil; event = window.PollEvent() {
				if event.Type == sf.EventKeyPressed && !disableInfo {
					switch event.Key.Code {
					case sf.KeyF1:
						showInfo = !showInfo

					case sf.KeyF2:
						vsync = !vsync
						window.SetVerticalSyncEnabled(vsync)
					}
				}
				game.Input(event)
			}
			game.Update(float32(dt))
			accumulator -= dt
		}

		// Render
		window.Clear(sf.ColorBlack)
		game.Draw(float32(accumulator / dt))
		if showInfo && !disableInfo {
			window.Draw(fpsText)
		}
		window.Display()

		// Calculate FPS
		fpsCounter++
		fpsAcc += frameTime
		if fpsAcc >= fpsMaxAcc {
			fpsAverage = float64(fpsCounter) / fpsAcc
			fpsAcc = 0
			fpsCounter = 0
			fpsText.SetString(fmt.Sprintf("VSYNC: %t\nFPS: %.0f", vsync, fpsAverage))
		}
	}

	// Clean stuff and close window
	game.Quit()
	window.Close()
}
