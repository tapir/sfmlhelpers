package main

import (
	"math/rand"

	sf "gitlab.com/tapir/sfml/v2.3/sfml"
	sh "gitlab.com/tapir/sfmlhelpers"
)

var bunnyRects = [...]sf.Recti{
	sf.Recti{2, 47, 26, 37},
	sf.Recti{2, 86, 26, 37},
	sf.Recti{2, 125, 26, 37},
	sf.Recti{2, 164, 26, 37},
	sf.Recti{2, 2, 26, 37},
}

var bunnyTexture = sf.NewTexture("bunnys.png")

type bunny struct {
	*sh.DrawElement
	sprite *sf.Sprite
	speed  sf.Vector2f
}

func newBunny(bunnyType int) *bunny {
	scale := rand.Float32()*0.5 + 0.5
	sprite := sf.NewSpriteFromRect(bunnyTexture, bunnyRects[bunnyType])
	sprite.SetOrigin(sf.Vector2f{0, 37})
	return &bunny{
		sh.NewDrawElement(&sh.DrawState{Scale: sf.Vector2f{scale, scale}, Angle: (rand.Float32() - 0.5) * 53}, sprite),
		sprite,
		sf.Vector2f{
			rand.Float32() * 10,
			rand.Float32()*10 - 5,
		},
	}
}
