package main

import (
	"runtime"

	sf "gitlab.com/tapir/sfml/v2.3/sfml"

	sh "gitlab.com/tapir/sfmlhelpers"
)

func init() {
	runtime.LockOSThread()
}

func main() {
	window := sf.NewRenderWindow(sf.VideoMode{800, 600, 32}, "Bunnymark", sf.StyleClose, nil)
	b := new(bunnyGame)
	sh.GameLoop(window, b, false)
}
