package main

import (
	"fmt"
	"math/rand"

	sf "gitlab.com/tapir/sfml/v2.3/sfml"
	sh "gitlab.com/tapir/sfmlhelpers"
)

type bunnyGame struct {
	window      *sf.RenderWindow
	bunnies     []*bunny
	maxX        float32
	minX        float32
	maxY        float32
	minY        float32
	isAdding    bool
	amount      int
	totalAmount int
	shouldQuit  bool
	text        *sf.Text
	bunnyType   int
	bg          *sf.RectangleShape
}

func (b *bunnyGame) Init(w *sf.RenderWindow) {
	fmt.Println("Bunnymark starting...")
	fmt.Println("Press mouse button to add bunnies")
	fmt.Println("Press F1 to show/hide info")
	fmt.Println("Press F2 to enable/disable vsync")
	b.window = w
	b.bunnies = append(b.bunnies, newBunny(b.bunnyType))
	b.bunnies = append(b.bunnies, newBunny(b.bunnyType))
	b.maxX = float32(b.window.GetSize().X)
	b.maxY = float32(b.window.GetSize().Y)
	b.amount = 100
	b.totalAmount = 2
	b.text = sh.NewDebugText(fmt.Sprintf("Total Bunnies: %d\n", b.totalAmount), 20)
	b.text.SetPosition(sf.Vector2f{4, 40})
	b.text.SetColor(sf.ColorYellow)
	b.bg = sf.NewRectangleShape(sf.Vector2f{165, 85})
	b.bg.SetPosition(sf.Vector2f{0, 0})
	b.bg.SetFillColor(sf.Color{0, 0, 255, 150})
}

func (b *bunnyGame) Update(dt float32) {
	if b.isAdding {
		for i := 0; i < b.amount; i++ {
			b.bunnies = append(b.bunnies, newBunny(b.bunnyType))
		}
		b.totalAmount += b.amount
		b.text.SetString(fmt.Sprintf("Total Bunnies: %d\n", b.totalAmount))
	}
	for _, v := range b.bunnies {
		ns := v.CurrentState
		ns.Position.X = ns.Position.X + v.speed.X
		ns.Position.Y = ns.Position.Y + v.speed.Y
		v.speed.Y += 0.5
		if ns.Position.X > b.maxX {
			v.speed.X *= -1
			ns.Position.X = b.maxX
		} else if ns.Position.X < b.minX {
			v.speed.X *= -1
			ns.Position.X = b.minX
		}
		if ns.Position.Y > b.maxY {
			v.speed.Y *= -0.85
			ns.Position.Y = b.maxY
			ns.Angle = (rand.Float32() - 0.5) * 25
			if rand.Float32() > 0.5 {
				v.speed.Y -= rand.Float32() * 6
			}
		} else if ns.Position.Y < b.minY {
			v.speed.Y = 0
			ns.Position.Y = b.minY
		}
		v.SetState(&ns)
	}
}

func (b *bunnyGame) Input(event *sf.Event) {
	switch event.Type {
	case sf.EventMouseButtonPressed:
		b.isAdding = true
		b.bunnyType = rand.Intn(5)
	case sf.EventMouseButtonReleased:
		b.isAdding = false
	case sf.EventClosed:
		b.shouldQuit = true
	case sf.EventKeyReleased:
		switch event.Key.Code {
		case sf.KeyEscape:
			b.shouldQuit = true
		}
	}
}

func (b *bunnyGame) Draw(alpha float32) {
	for _, v := range b.bunnies {
		v.Draw(b.window, alpha)
	}
	b.window.Draw(b.bg)
	b.window.Draw(b.text)
}

func (b *bunnyGame) Quit() {
	fmt.Println("Bunnymark quitting...")
}

func (b *bunnyGame) IsRunning() bool {
	return !b.shouldQuit
}
