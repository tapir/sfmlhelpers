Helpers to use with SFML
========================

* `GameLoop()` method accepts a `Game` interface. It implements the *maximum speed FPS with fixed timestep method*,
as mentioned in [this](http://gafferongames.com/game-physics/fix-your-timestep/) article.
* `DrawElement` can be used as an anonymous field on game entities to automate state interpolation for the `GameLoop()`. Angle interpolation takes the shortest path.
* See **examples/bunnymark** for real world usage.
