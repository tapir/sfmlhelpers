package sfmlhelpers

import (
	sf "gitlab.com/tapir/sfml/v2.3/sfml"
)

var debugFont = sf.NewFontFromMemory(clacon)

// NewDebugText creates a new text with a default monospaced font.
func NewDebugText(text string, size uint) *sf.Text {
	return sf.NewText(text, debugFont, size)
}
