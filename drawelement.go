package sfmlhelpers

import sf "gitlab.com/tapir/sfml/v2.3/sfml"

// DrawState holds information about the position, scale and angle of an entity.
type DrawState struct {
	Position sf.Vector2f
	Scale    sf.Vector2f
	Angle    float32
}

// DrawElement holds information about last and current state of the entity.
type DrawElement struct {
	drawable     sf.Drawable
	lastState    DrawState
	CurrentState DrawState
}

// NewDrawElement creates a new DrawElement from a state and drawable reference.
func NewDrawElement(currentState *DrawState, drawable sf.Drawable) *DrawElement {
	r := new(DrawElement)
	r.CurrentState = *currentState
	r.drawable = drawable
	r.SetState(currentState)
	return r
}

// SetState updates the last and current states.
func (d *DrawElement) SetState(state *DrawState) {
	d.lastState = d.CurrentState
	d.drawable.SetPosition(state.Position)
	d.drawable.SetScale(state.Scale)
	d.drawable.SetRotation(state.Angle)
	d.CurrentState = *state
}

// Only updates position
func (d *DrawElement) SetPosition(position sf.Vector2f) {
	d.lastState = d.CurrentState
	d.drawable.SetPosition(position)
	d.CurrentState.Position = position
}

// Only updates scale
func (d *DrawElement) SetScale(scale sf.Vector2f) {
	d.lastState = d.CurrentState
	d.drawable.SetScale(scale)
	d.CurrentState.Scale = scale
}

// Only updates angle
func (d *DrawElement) SetAngle(angle float32) {
	d.lastState = d.CurrentState
	d.drawable.SetRotation(angle)
	d.CurrentState.Angle = angle
}

// Draw draws a drawable with interpolated state.
func (d *DrawElement) Draw(w *sf.RenderWindow, alpha float32) {
	d.interpolate(alpha)
	w.Draw(d.drawable)
}

func (d *DrawElement) interpolate(alpha float32) {
	position := sf.Vector2f{
		d.lastState.Position.X + (d.CurrentState.Position.X-d.lastState.Position.X)*alpha,
		d.lastState.Position.Y + (d.CurrentState.Position.Y-d.lastState.Position.Y)*alpha,
	}
	scale := sf.Vector2f{
		d.lastState.Scale.X + (d.CurrentState.Scale.X-d.lastState.Scale.X)*alpha,
		d.lastState.Scale.Y + (d.CurrentState.Scale.Y-d.lastState.Scale.Y)*alpha,
	}
	angle := d.lastState.Angle + angleDelta(d.lastState.Angle, d.CurrentState.Angle)*alpha
	d.drawable.SetPosition(position)
	d.drawable.SetScale(scale)
	d.drawable.SetRotation(angle)
}

func angleDelta(lastAngle, currentAngle float32) float32 {
	angularChange := currentAngle - lastAngle
	if angularChange <= 180 && angularChange >= -180 {
		return angularChange
	} else if angularChange > 180 {
		return (angularChange - 360)
	} else {
		return (angularChange + 360)
	}
}
